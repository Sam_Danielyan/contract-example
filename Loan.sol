// SPDX-License-Identifier: MIT
pragma solidity ^0.6.12;
pragma experimental ABIEncoderV2;

import "@aave/protocol-v2/contracts/interfaces/ILendingPoolAddressesProvider.sol";
import "@aave/protocol-v2/contracts/flashloan/base/FlashLoanReceiverBase.sol";
import "@aave/protocol-v2/contracts/interfaces/ILendingPool.sol";

import "@uniswap/v2-periphery/contracts/interfaces/IUniswapV2Router02.sol";
import "contracts/interface/IUniswapV2ERC20.sol";
import "hardhat/console.sol";

/** 
    !!!
    Never keep funds permanently on your FlashLoanReceiverBase contract as they could be 
    exposed to a 'griefing' attack, where the stored funds are used by an attacker.
    !!!
 */

contract Loan is FlashLoanReceiverBase {
    using SafeMath for uint256;

    string greeting;

    IUniswapV2Router02 uniswapRouter = IUniswapV2Router02(0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D);
    address[] coins = new address[](2);
    address[] toSwapCoins = new address[](2);


    constructor(ILendingPoolAddressesProvider _addressProvider) public FlashLoanReceiverBase(_addressProvider) {}
    /**
        This function is called after your contract has received the flash loaned amount
     */
    function executeOperation(
        address[] calldata assets,
        uint256[] calldata amounts,
        uint256[] calldata premiums,
        address initiator,
        bytes calldata params
    ) external override returns (bool) {
        toSwapCoins[0] = address(0x6B175474E89094C44Da98b954EedeAC495271d0F); //DAI
        toSwapCoins[1] = address(0x7Fc66500c84A76Ad7e9c93437bFc5Ac33E2DDaE9); //AAVE


        // Approve the LendingPool contract allowance to *pull* the owed amount
        for (uint256 i = 0; i < assets.length; i++) {
            uint256 amountOwing = amounts[i].add(premiums[i]);
            coins[0] = address(0x6B175474E89094C44Da98b954EedeAC495271d0F);

            IERC20(assets[i]).approve(address(uniswapRouter), 50 ether);
            uniswapRouter.swapExactTokensForTokens(50 ether, 0, toSwapCoins, msg.sender, block.timestamp + 60);

            IERC20(assets[i]).approve(address(LENDING_POOL), 500 ether);
        }
        return true;
    }


    function myFlashLoanCall() public payable {
        coins[0] = address(0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2);
        coins[1] = address(0x6B175474E89094C44Da98b954EedeAC495271d0F);

        uniswapRouter.swapExactETHForTokens{value: 1 ether}(0, coins, address(this), block.timestamp + 50);

        uint256[] memory amounts = new uint256[](1);
        address[] memory assets = new address[](1);
        uint256[] memory modes = new uint256[](1);
        address receiverAddress = address(this);
        address onBehalfOf = address(this);
        bytes memory params = "";
        uint16 referralCode = 0;

        assets[0] = address(0x6B175474E89094C44Da98b954EedeAC495271d0F); // DAI
        amounts[0] = 200 ether;
        modes[0] = 0;

        LENDING_POOL.flashLoan(receiverAddress, assets, amounts, modes, onBehalfOf, params, referralCode);
    }
}

